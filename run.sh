#!/bin/bash
/opt/ODK-Aggregate-v${AGG_VERS}-Linux-x64.run \
            --parent_installdir $CATALINA_HOME/webapps \
            --hostname $HOST_NAME \
            --database_hostname $DB_HOST \
            --jdbc_username $DB_USER \
            --jdbc_password $DB_PASSWORD


