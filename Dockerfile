FROM tomcat:8.5.54-jdk11-openjdk

WORKDIR /opt

ARG AGG_VERS=2.0.5
ARG CATALINA_HOME=/usr/local/tomcat

WORKDIR /opt

RUN wget -O aggregate.zip https://github.com/getodk/aggregate/releases/download/v${AGG_VERS}/ODK-Aggregate-v${AGG_VERS}-Linux-x64.run.zip

RUN unzip aggregate.zip

RUN rm aggregate.zip

COPY run.sh /opt

RUN chmod +x /opt/run.sh

RUN echo "Please run /opt/run.sh to configure aggregate"

WORKDIR $CATALINA_HOME
