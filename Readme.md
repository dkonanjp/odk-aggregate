# ODK Docker-compose

This is a docker-compose setup for [ODK](https://getodk.org)

## Install


1. Adapt config `cp .env.sample .env` and edit `.env` in particular the DB password
2. Build and start : `docker-compose up --build -d` (this step may take a while)
3. Build the `.war file` : `docker-compose exec front /opt/run.sh` and answer questions
    + Say no to all aditional installation
    + Accept licences
    + Configuration without SSL
    + Database postgresql
    + Most defaults are set according to .env
4. Get the sql `docker-compose exec front cat webapps/ODK\ Aggregate/create_db_and_user.sql`
5. Run `docker-compose exec db psql -U aggregate` and paste SQL from step 4
6. Run `docker-compose exec front mv webapps/ODK\ Aggregate/ODKAggregate.war webapps/ROOT.war`
7. Restart the docker-compose `docker-compose down; docker-compose up -d`

At this point everything should be configured, go to `http://localhost:8080/` and finish installation process.

Default password is `aggregate`, username is the one you set in step 2.

## Use it behing an apache2 proxy

1. Follow install instruction using a Fully Qualified Domain Name in step 2 instead of `localhost`
2. Copy the odk.conf file to `/etc/apache2/sites-available/odk.conf` and adapt it
3. `a2ensite odk.conf && apache2ctl graceful`
3. **Recommended** switch to HTTPS using [certbot](https://certbot.eff.org/)
